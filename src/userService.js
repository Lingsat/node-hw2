const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('./model/User');
require('dotenv').config();

const registerUser = async (req, res, next) => {
  const { username, password } = req.body;
  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
    createdDate: new Date().toISOString(),
  });
  user
    .save()
    .then(() => {
      res.status(200).json({ message: 'Success' });
    })
    .catch((err) => next(err));
};

const loginUser = async (req, res) => {
  const user = await User.findOne({ username: req.body.username });
  if (
    user
    && (await bcrypt.compare(String(req.body.password), String(user.password)))
  ) {
    const payload = { username: user.username, userId: user.id };
    const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);
    return res.json({ message: 'Success', jwt_token: jwtToken });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

const getUserProfile = (req, res) => (
  User.findById(req.user.userId).then((member) => {
    if (member) {
      res.json({
        user: {
          _id: req.user.userId,
          username: member.username,
          createdDate: member.createdDate,
        },
      });
    } else {
      res.status(400).json({
        message:
          'You do not have access to profile information or information not found!',
      });
    }
  })
);

const delUserProfile = (req, res) => (
  User.findByIdAndDelete(req.user.userId).then(() => (
    res.json({ message: 'User deleted successfully!' })
  ))
);

const changeUserPassword = async (req, res) => {
  const newPass = await bcrypt.hash(req.body.newPassword, 10);
  await User.findByIdAndUpdate(req.user.userId, {
    $set: { password: newPass },
  }).then(() => res.json({ message: 'Password changed' }));

  // const currentUser = await User.findById(req.user.userId);
  // if (currentUser && await bcrypt.compare(String(req.body.oldPassword),
  // String(currentUser.password))) {
  //   const newPass = await bcrypt.hash(req.body.newPassword, 10);
  //   currentUser.password = newPass;
  //   currentUser.save();
  //   res.json({ message: 'Password changed' })
  // } else {
  //   return res.status(400).json({ message: 'Old Password incorrect!' })
  // }
};

module.exports = {
  registerUser,
  loginUser,
  getUserProfile,
  delUserProfile,
  changeUserPassword,
};
