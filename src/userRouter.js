const express = require('express');
const { authMiddleware } = require('./middleware/authMiddleware');
const {
  registerUser,
  loginUser,
  getUserProfile,
  delUserProfile,
  changeUserPassword,
} = require('./userService');

const router = express.Router();

router.post('/register', registerUser);
router.post('/login', loginUser);
router.get('/me', authMiddleware, getUserProfile);
router.delete('/me', authMiddleware, delUserProfile);
router.patch('/me', authMiddleware, changeUserPassword);

module.exports = { userRouter: router };
