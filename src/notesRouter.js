const express = require('express');
const {
  createNote,
  getUserNotes,
  getUserNoteById,
  updateUserNoteById,
  checkUncheckUserNoteById,
  delUserNoteById,
} = require('./notesService');
const { authMiddleware } = require('./middleware/authMiddleware');

const router = express.Router();

router.post('/', authMiddleware, createNote);
router.get('/', authMiddleware, getUserNotes);
router.get('/:id', authMiddleware, getUserNoteById);
router.put('/:id', authMiddleware, updateUserNoteById);
router.patch('/:id', authMiddleware, checkUncheckUserNoteById);
router.delete('/:id', authMiddleware, delUserNoteById);

module.exports = { notesRouter: router };
