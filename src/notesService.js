const { Note } = require('./model/Note');

const createNote = (req, res) => {
  const { text } = req.body;
  if (text && text !== '') {
    const note = new Note({
      text,
      userId: req.user.userId,
      createdDate: new Date().toISOString(),
    });
    note.save().then(() => {
      res.status(200).json({ message: 'Success' });
    });
  } else {
    res.status(400).json({ message: 'Bad request!' });
  }
};

const getUserNotes = (req, res) => Note.find({ userId: req.user.userId }).then((list) => {
  res.json({
    offset: 0,
    limit: 5,
    count: list.length,
    notes: list,
  });
});

const getUserNoteById = (req, res) => Note.findOne({ _id: req.params.id, userId: req.user.userId })
  .then((item) => {
    if (item) {
      res.json({
        note: item,
      });
    } else {
      res
        .status(400)
        .json({ message: "You don't have access to get this note!" });
    }
  })
  .catch(() => res.status(400).json({ message: 'Not Found!' }));

const updateUserNoteById = (req, res) => Note.findOneAndUpdate(
  { _id: req.params.id, userId: req.user.userId },
  { $set: { text: req.body.text } },
)
  .then((note) => {
    if (note) {
      res.json({ message: 'Updated successfully' });
    } else {
      res.status(400).json({
        message: 'You do not have access to update other user notes!',
      });
    }
  })
  .catch(() => res.status(400).json({
    message:
          'Not Found! Or you do not have access to update other user notes!',
  }));

const checkUncheckUserNoteById = async (req, res) => {
  const noteItem = await Note.findOne({
    _id: req.params.id,
    userId: req.user.userId,
  });
  if (noteItem) {
    noteItem.completed = !noteItem.completed;
    noteItem.save();
    res.json({ message: 'Changed successfully' });
  } else {
    res.status(400).json({
      message:
        'Not Found! Or you do not have access to update other user notes!',
    });
  }
};

const delUserNoteById = (req, res) => (
  Note.findOneAndDelete({ _id: req.params.id, userId: req.user.userId })
    .then((note) => {
      console.log(note);
      if (note) {
        res.json({ message: 'Deleted successfully' });
      } else {
        res.status(400).json({
          message: 'You do not have access to delete other user notes!',
        });
      }
    })
    .catch(() => res.status(400).json({
      message:
            'Not Found! Or you do not have access to delete other user notes!',
    }))
);

module.exports = {
  createNote,
  getUserNotes,
  getUserNoteById,
  updateUserNoteById,
  checkUncheckUserNoteById,
  delUserNoteById,
};
