const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  text: {
    type: String,
    require: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    require: true,
  },
  createdDate: {
    type: String,
    require: true,
  },
});

const Note = mongoose.model('note', noteSchema);

module.exports = { Note };
