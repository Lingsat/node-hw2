NodeJS Server gitLab repository https://gitlab.com/Lingsat/node-hw2

UI https://lingsat.github.io/TodoEpam/

To start testing you need to download server repository and in terminal:
1) npm install - to install add node_modules
2) npm start - to run "node index.js"

Author: Serhii Petrenko 
Email: lingsatpilingast@gmail.com
