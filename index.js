const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();
const bodyParser = require('body-parser');

const { notesRouter } = require('./src/notesRouter');
const { userRouter } = require('./src/userRouter');

const app = express();
const PORT = process.env.PORT || 8080;

mongoose.connect(
  'mongodb+srv://lingsat:mypassword@cluster0.g6azfv6.mongodb.net/usernotesapp?retryWrites=true&w=majority',
);

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));

// body parcer
app.use(bodyParser.json());

// cors headers
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', req.headers.origin);
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use('/api/notes', notesRouter);
app.use('/api/auth', userRouter);
app.use('/api/users', userRouter);

app.listen(PORT);

// ERROR HANDLER
function errorHandler(err, req, res) {
  // console.error(err);
  res.status(500).send({ message: 'Server error' });
}
app.use(errorHandler);
